package com.company;

import java.util.Scanner;

class Persegi implements Operasi1{
    double s;

    @Override
    public double Luas(){
        return s*s;
    }
}

class PersegiPanjang implements Operasi1{
    double p, l;

    @Override
    public double Luas(){
        return p*l;
    }
}

class Balok implements Operasi1,Operasi2{
    double p, l, t;

    @Override
    public double Luas(){
        return 2*((p*l)+(p*t)+(l*t));
    }

    @Override
    public void Volume(){
        double hasil;
        hasil= p*l*t;
        System.out.println("Volume balok : "+hasil);
    }
}

interface Operasi1{
    double Luas();
}

interface Operasi2{
    void Volume();
}

public class Main{

    public static void main(String[] args){
        //write your code here
        int p1, p2;
        Scanner in = new Scanner(System.in);
        Persegi psg = new Persegi();
        PersegiPanjang psgpnj = new PersegiPanjang();
        Balok blk = new Balok();

        System.out.println("Pilih Jenis Bangun Datar/Ruang");
        System.out.println("1. Persegi");
        System.out.println("2. Persegi Panjang");
        System.out.println("3. Balok");
        System.out.println("Masukkan pilihan : ");
        p1 = in.nextInt();
        switch(p1){
            case 1:
                System.out.println("\nAnda memilih Persegi");
                System.out.println("Masukkan sisi : ");
                psg.s = in.nextDouble();
                System.out.println("1.Cari Luas");
                p2 = in.nextInt();
                if(p2==1){
                    System.out.print("Luas Persegi :"+psg.Luas());
                }
                break;
            case 2:
                System.out.println("\nAnda memilih Persegi Panjang");
                System.out.print("Masukkan Panjang : ");
                psgpnj.p = in.nextDouble();
                System.out.print("Masukkan Lebar : ");
                psgpnj.p = in.nextDouble();
                System.out.print("1.Cari Luas : ");
                p2 = in.nextInt();
                if(p2==1){
                    System.out.print("Luas Persegi Panjang : "+psgpnj.Luas());
                }
                break;
            case 3:
                System.out.println("\nAnda memilih Balok");
                System.out.print("Masukkan Panjang : ");
                blk.p = in.nextDouble();
                System.out.print("Masukkan Lebar : ");
                blk.l = in.nextDouble();
                System.out.print("Masukkan Tinggi : ");
                blk.t = in.nextDouble();
                System.out.print("1.Cari Luas");
                System.out.print("2.Cari Volume");
                System.out.print("Masukkan Pilihan : ");
                p2 = in.nextInt();
                if(p2==1){
                    System.out.print("Luas Balok : "+blk.Luas());
                }else if(p2==2){
                    blk.Volume();
                }
                break;
            default:
                System.out.println("Input salah");
        }
        System.out.println();
    }
}
